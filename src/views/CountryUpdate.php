<!DOCTYPE html>
<html lang="en">
    <head>
        <title>CountryUpdate</title>
        <?php include('Head.php'); ?>
    </head>
    <body>
        
        <br>
        <form method="POST" action="/public_html/country/update/<?php echo $countries[0][2] ?>">
            <h3 class="text-center text" name="txt">Editer un Pays</h3>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 offset-sm-3 mt-5">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">id : </span>
                            </div>
                            <input type="number" class="form-control" name="id" value="<?php echo $countries[0][0] ?>" readonly="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Nom : </span>
                            </div>
                            <input type="text" class="form-control" name="nom" value="<?php echo $countries[0][2] ?>" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Code : </span>
                            </div>
                            <input type="text" pattern="[A-Z]{3}" class="form-control" name="code" value="<?php echo $countries[0][1] ?>" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Continent : </span>
                            </div>
                            <input type="text" class="form-control" name="continent" value="<?php echo $countries[0][3] ?>" readonly="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Region : </span>
                            </div>
                            <input type="text" class="form-control" name="region" value="<?php echo $countries[0][4] ?>" required="">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Population : </span>
                            </div>
                            <input type="number" class="form-control" name="population" value="<?php echo $countries[0][7] ?>" required="">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Capitale : </span>
                            </div>
                            <input type="number" class="form-control" name="capital" value="<?php echo $countries[0][14] ?>" required="">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Surface Area : </span>
                            </div>
                            <input type="number" class="form-control" name="surfaceArea" value="<?php echo $countries[0][5] ?>" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Independance Year : </span>
                            </div>
                            <input type="number" pattern="" class="form-control" name="indepYear" value="<?php echo $countries[0][6] ?>">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Life Expectancy : </span>
                            </div>
                            <input type="number" class="form-control" name="lifeExpectancy" value="<?php echo $countries[0][8] ?>">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">GNP : </span>
                            </div>
                            <input type="number" class="form-control" name="gnp" value="<?php echo $countries[0][9] ?>" required="">
                        </div> 

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">GNPOld : </span>
                            </div>
                            <input type="number" class="form-control" name="gnpOld" value="<?php echo $countries[0][10] ?>">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Local Name : </span>
                            </div>
                            <input type="text" class="form-control" name="localName" value="<?php echo $countries[0][11] ?>" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Government Form : </span>
                            </div>
                            <input type="text" class="form-control" name="governmentForm" value="<?php echo $countries[0][12] ?>" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Head of State : </span>
                            </div>
                            <input type="text" class="form-control" name="headOfState" value="<?php echo $countries[0][13] ?>" required="">
                        </div> 

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Code2 : </span>
                            </div>
                            <input type="text" class="form-control" name="code2" value="<?php echo $countries[0][15] ?>">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Image1 : </span>
                            </div>
                            <input type="text" class="form-control" name="image1" value="<?php echo $countries[0][16] ?>">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Image2 : </span>
                            </div>
                            <input type="text" class="form-control" name="image2" value="<?php echo $countries[0][17] ?>">
                        </div>
                        
                       
                                             <input type="submit" name="valid" value="valider">
                        
                </div>

            </div>
        </form>
    </body>
</html>