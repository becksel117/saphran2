<!DOCTYPE html>
<html lang="en">
    <head>
        <title>CityAdd</title>
        <?php include('Head.php'); ?>
    </head>
    <body>
        
        <br>
        <form method="POST" >
            <h3 class="text-center text" name="txt">Ajouter une Ville</h3>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 offset-sm-3 mt-5">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">id : </span>
                            </div>
                            <input type="number" class="form-control" name="id" value="" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Nom : </span>
                            </div>
                            <input type="text" class="form-control" name="nom" value="" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Code : </span>
                            </div>
                            <input type="text" pattern="[A-Z]{3}" class="form-control" name="code" value="" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">District : </span>
                            </div>
                            <input type="text" class="form-control" name="district" value="" required="">
                        </div>   
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Population : </span>
                            </div>
                            <input type="text" class="form-control" name="population" value="" required="">
                        </div>   

                       
                       
                                             <input type="submit" name="valid" value="valider">
                        
                </div>

            </div>
        </form>
    </body>
</html>
