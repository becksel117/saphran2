<!DOCTYPE html>

<html lang="en">
    <head>
        <title>City.php</title>
        <?php include('Head.php'); ?>
         <link rel="stylesheet" href="/src/views/styles.css">
    </head>
    
    <body>
        

        <br>
        
  <h2 class="text-center">Détails de <?php echo $countries[0][2]; ?></h2>

  

  <div class="row justify-content-md-center">
    <div class="col-10">

      <table class="table table-sm">
        <tbody>
          <tr>
            <th scope="col">id</th>
            <td><?= $countries[0][1]; ?></td>
          </tr>
        </tbody>
        <tbody>
          <tr>
            <th scope="row">nom</th>
            <td><?= $countries[0][2]; ?></td>

          </tr>
          <tr>
            <th scope="row">Continent</th>
            <td><?= $countries[0][3]; ?></td>

          </tr>
          <tr>
            <th scope="row">Capital</th>
            <td><?= $countries[0][14]; ?></td>

          </tr>
          <tr>
            <th scope="row">Population</th>
            <td><?= $countries[0][7]; ?></td>
          </tr>
        </tbody>
      </table>

</div>
</div>

      <div class="row justify-content-md-center">
        <div class="col-10">

          <table class="table table-striped">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">id</th>
                <th scope="col">nom</th>
                <th scope="col">district</th>
                <th scope="col">population</th>
                 
                <th scope="col"><a href="/public_html/city/add/<?php echo $countries[0][2]; ?>">Ajouter</a></th>
              </tr>
            </thead>
            
            <?php
                $id = 0; 
                foreach ($cities as $city) { 
                    $id ++;
            ?>
                <tbody>
                    <tr>
                        <td><?php echo $id; ?></td>
                        <td><?php echo $city['City_Id']; ?></a></td>
                        <td><?php echo $city['Name']; ?></td>
                        <td><?php echo $city['District']; ?></td>
                        <td><?php echo $city['Population']; ?></td>
                        <td><a href="/public_html/city/update/<?php echo $city['Name'];?>">
                                            <input type="submit" name="edit" value="éditer">
                                        </a>
                        <a href="/public_html/city/delete/<?php echo $city['Name']; ?>">
                                            <input type="submit" name="suppr" value="supprimer">
                                        </a>
                        </td>
                      </tr>
                </tbody>
            <?php
                }
            ?>
            </table>
            <br>
        </div>
    </body>
</html>