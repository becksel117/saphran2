<!DOCTYPE html>

<html lang="en">
    <head>
        <title>Liste des pays</title>
        <?php include('Head.php'); ?>
         
         <style>
        img {
            width: 90px;
            height: 50px;
        }
        </style>
    </head>
    
    <body>
        
        <br>
        
        <div class="container">
            <h2 class="text-center">Les pays de <?php echo $country[0]['Continent']; ?></h2>
            <br>
                <table class="table table-striped ">
            <br>
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Id</th>
                        <th scope="col">Drapeau</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Code</th>
                        <th scope="col">Region</th>
                        <th scope="col">Capitale</th>
                        <th scope="col">Population</th>
                        <th scope="col"><a href="/public_html/country/add/<?php echo $country[0]['Continent']; ?>">Ajouter</a></th>
                    </tr>
                </thead>
          
            <tbody>
                        <?php
                        
                        $scope = $page * 10 - 10;
                        for ($indent = $page * 10 - 10; $indent < ceil($page * 10); $indent++): $scope++; 
                            ?>
                            <?php if (isset($country[$indent])): ?>
                                <tr>
                                    <th scope="row"><?= $scope; ?></th>
                                    <td><?= $country[$indent]["Country_Id"]; ?></td>
                                    <td><img class="drapeau" src="<?php
                                        if ($country[$indent]["Image1"] != null) {
                                            echo $country[$indent]["Image1"];
                                        } else if ($country[$indent]['Image2'] != null) {
                                            echo $country[$indent]["Image2"];
                                        } else {
                                            echo "";
                                        }
                                        ?>"></td>
                                    <td><a href="/public_html/city/show/<?= $country[$indent]["Name"]; ?>?page=1"><?= $country[$indent]["Name"]; ?></a></td>
                                    <td><?= $country[$indent]["Code"]; ?></td>
                                    <td><?= $country[$indent]["Region"]; ?></td>
                                    <td><?= $country[$indent]["Capital"]; ?></td>
                                    <td><?= $country[$indent]["Population"]; ?></td>
                                    <td>
                                        <a href="/public_html/country/update/<?php echo $country[$indent]['Name'];?>">
                                            <input type="submit" name="edit" value="éditer">
                                        </a>
                                        <a href="/public_html/country/delete/<?php echo $country[$indent]['Name'];?>">
                                            <input type="submit" name="suppr" value="supprimer">
                                        </a>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tbody>

                
            </table>
            
             <div class="row justify-content-md-center">
                    <div class="col col-lg-3">
                        <nav aria-label="...">
                            <?php 
                            $paginator;
                            
                            ?>
                        </nav>
                    </div>
                </div>

            <br>
        </div>
    </body>
</html>

