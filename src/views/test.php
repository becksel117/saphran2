<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Accueil</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>
    
    
    <body>
        <?php 
        require_once '../src/controllers/DefaultController.php';
        require_once '../src/utils/SingletonDatabase.php';
        require_once '../src/controllers/BaseController.php';
        require_once '../src/model/DAOCountry.php';

        ?>

    <br>
    <br>
        <?php
            foreach ($continent as $value) {
        ?>
        <div class="card-deck">
            <div class="card" style="width:150px">
                <img class="card-img-top" src="https://www.lyonmag.com/medias/images/lyon-4k.jpg" alt="Card image">
                <div class="card-body">
                    <h4 class="card-title" style="text-align: center"><?php echo $value['continent'] ?></h4>
                    
                    <a href="#" class="stretched-link"></a>
                </div>
            </div>
        </div>
    <?php
            }
    ?>
    
    
    <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="..." alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
        </div>
    </div>
    
    
    
    
    
    </body>
</html>
