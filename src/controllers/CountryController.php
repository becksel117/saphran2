<?php
require_once '../src/controllers/BaseController.php';
require_once '../src/utils/Renderer.php';
require_once '../src/model/DAOCountry.php';
require_once '../src/utils/Pagination.php';

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CountryController extends BaseController {
    use Pagination;
    private $DAOCountry ;
    private $country;
    
    public function __construct() {
        $this->DAOCountry = new DAOCountry(SingletonDataBase::getInstance()->cnx);
       
    }
    
    public function test($alpha){
        $page = Renderer::render ('demo.php', compact($alpha));
        echo $page;
        
    } 
    
    
    public function show(){
        $country = $this->DAOCountry->Find();
        $DAOCountry = Renderer::render ('Countries.php', compact('country'));
        echo $DAOCountry;
    }
    
    public function display($continent = null) {
        if ($continent != null) {
            $country = $this->DAOCountry->FindCountriesByContinent($continent);
        } else {
            
            $country = $this->DAOCountry->findAll();
        }
        $page = 1;
        $paginator = $this->paginate($this->DAOCountry->count() , 10);
        $value = Renderer::render("Countries.php", compact("country","page","paginator"));
        echo $value;
        
    }
    
    public function update($country = null) {
        $countries = $this->DAOCountry->FindCountryByCountry($country);
        $value = Renderer::render("CountryUpdate.php", compact("country","countries"));
        echo $value;
    }
    
    public function delete($country = null) {
        $countries = $this->DAOCountry->FindCountryByCountry($country);
        $value = Renderer::render("CountryDelete.php", compact("country","countries"));
        echo $value;
    }
    
    public function doDelete($countries) {
        remove($countries[0][0]);
        var_dump($countries);
    }
    
    public function add($country = null) {
        $countries = $this->DAOCountry->FindCountryByCountry($country);
        $value = Renderer::render("CountryAdd.php", compact("country","countries"));
        echo $value;
    }
    
}



