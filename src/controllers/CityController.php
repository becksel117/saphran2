<?php
require_once '../src/controllers/BaseController.php';
require_once '../src/utils/Renderer.php';
require_once '../src/utils/SingletonDatabase.php';
require_once '../src/utils/SingletonConfigReader.php';
require_once '../src/model/DAOCity.php';
require_once '../src/model/DAOCountry.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CityController
 *
 * @author student
 */
class CityController extends BaseController {
    
    private $daoCity ;
    private $city;
    private $DAOCountry;
    
    public function __construct() {
        $this->daoCity = new DAOCity(SingletonDataBase::getInstance()->cnx);
        $this->DAOCountry = new DAOCountry(SingletonDataBase::getInstance()->cnx);
    }
    
    public function test($alpha){
        $page = Renderer::render ('demo.php', compact($alpha));
        echo '$page';
        
    } 
    public function showCity($id) {
        $city = $this->daoCity->Find();
        $DAOCity = Renderer::render ('City.php', compact('city'));
        echo $DAOity;
        
        
    }
    /***
     * Affichage d'une ou des villes
     */
    public function display($country = null) {
        if ($country != null) {
            $cities = $this->daoCity->FindCitiesByCountry($country);
        } else {
            $cities = $this->daoCity->findAll();
        }
        $page = 1;
        $pagination = 1;
        $countries = $this->DAOCountry->FindCountryByCountry($country);
        
        $page = Renderer::render("City.php", compact("country","page","pagination","countries","cities"));
        echo $page;
    }  
    /***
     * MAJ d'une ville
     */
    public function update($cities = null) {
        $city = $this->daoCity->FindCitiesByCity($cities);
        $value = Renderer::render("CityUpdate.php", compact("cities","city"));
        echo $value;
    }
    public function delete($country = null) {
        $cities = $this->daoCity->FindCitiesByCountry($country);
        $countries = $this->DAOCountry->FindCountryByCountry($country);
        $value = Renderer::render("CityDelete.php", compact("country","countries","cities"));
        echo $value;
    }
    public function add($country = null) {
        $cities = $this->daoCity->FindCitiesByCountry($country);
        $countries = $this->DAOCountry->FindCountryByCountry($country);
        $value = Renderer::render("CityAdd.php", compact("country","countries","cities"));
        echo $value;
    }
}
