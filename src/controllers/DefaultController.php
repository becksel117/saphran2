<?php
require_once '../src/controllers/BaseController.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DefaultController
 *
 * @author student
 */
class DefaultController extends BaseController {
    private $DAOCountry;
    public function __construct() {
        $this->DAOCountry = new DAOCountry(SingletonDataBase::getInstance()->cnx);
       
    }
    public function show() {
        $continent = $this->DAOCountry->FindContinent();
        $page = Renderer::render ('Accueil.php', compact('continent'));
        echo $page;
    }
}
