<?php

class SingletonConfigReader {
   
    private static $pathfile;
    public $config;
    
    /** @var SingletonDatabase $instance */
    private static $instance = null;
    
    
    
   
    private function __construct() {  
        
        self::$pathfile = '../config.ini';
        $this->config = parse_ini_file(self::$pathfile, true);
        
    }
    
    public static function getInstance()  : SingletonConfigReader{
 
        if(self::$instance == null) {
        self::$instance = new SingletonConfigReader();  
        }
        return self::$instance;
    }
    
    
    public function getValue(string $key, string $section = null): ?string {
    if ($section !=null)
        {
            return $this->config[$section][$key];
        }
        else {
            return $this->config[$key];
        }
    }
     
    
   
}
