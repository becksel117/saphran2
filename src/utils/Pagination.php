<?php

/**
 * Description of Pagination
 *
 * @author student
 */
trait Pagination {

    /**
     * 
     * @param string $uri : la base des urls pour generer les liens
     * @param int $page : la page courante (ou l'on se trouve)
     * @param int $total_items
     * @param int $items_per_page
     */
    public function paginate(int $total_items, int $items_per_page) {

        $nbPages = $total_items / $items_per_page;
        $i = 0;
        $numéroPage = 1;
        echo '<nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                    </li>';
        while ($i < $nbPages) {
            $pagination = <<< page
                    <li class="page-item"><a class="page-link" href="#">$numéroPage</a></li>
                    page;
            $i = $i + 1;
            $numéroPage = $numéroPage + 1;
            echo $pagination;
        }
        echo '<li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Next</a>
              </li>
                    </ul>
                    </nav>';

}
}
