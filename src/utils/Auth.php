<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Auth
 *
 * @author student
 */
class Auth {
    static $KEY="USERSESSION" ;
    /**
     * Store User Object in session
     * @param User $user
     */
    public static function login(User $user) {
        
    }
    /**
     * Logout the current user (if logged)
     */
    public static function logout() {
        
    }
    
    /**
     * check if current connection belongs to a logged user
     * @return bool
     */
    public static function isLogged() : bool {
        
    /**
     * check if user has role
     * @param string $role
     * @return bool
     */
    }
    public static function hasRole(string $role) : bool {
        
    }
    
    /**
     * check if current user has required permission
     * @param int $perm
     * @return bool
     */
    public static function can(int $perm) : bool {
        
    }
}


if (Auth::can(Auth::$CANCREATE)) {
    
}