<?php
require_once '../utils/AbstractVisitor.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PasswordVisitor
 *
 * @author student
 */
class PasswordVisitor extends AbstractVisitor {
    public function visite(string $data): bool { 
        $password = (string) $data;
        if(strlen($password) > 6 && preg_match('#[A-Z ]#',$password) && preg_match('@[$#!?]@',$password)){
            return true;
        }
        else {
            return false;
        }
    }
} 
