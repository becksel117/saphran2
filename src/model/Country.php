<?php

require_once '../src/model/Model.php';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Country extends Model {
 
    protected $Country_Id;
    protected $Code;
    protected $Name;
    protected $Continent;
    protected $Region;
    protected $SurfaceArea;
    protected $IndepYear;
    protected $Population;
    protected $LifeExpectancy;
    protected $GNP;
    protected $GNPOld;
    protected $LocalName;
    protected $GovernementForm;
    protected $HeadOfState;
    protected $Capital;
    protected $Code2;
    protected $Image1;
    protected $Image2;


    /**
     * @param array $data
     */
    public function __construct(array $data = null) {
        parent::__construct();
        $this->Country_Id = $data["Country_Id"];
        $this->Code = $data["Code"];
        $this->Name = $data["Name"];
        $this->Continent = $data["Continent"];
        $this->Region = $data["Region"];
        $this->SurfaceArea = $data["SurfaceArea"];
        $this->IndepYear = $data["IndepYear"];
        $this->Population = $data["Population"];
        $this->LifeExpectancy = $data["LifeExpectancy"];
        $this->GNP = $data["GNP"];
        $this->GNPOld = $data["GNPOld"];
        $this->LocalName = $data["LocalName"];
        $this->GovernementForm = $data["GovernementForm"];
        $this->HeadOfState = $data["HeadOfState"];
        $this->Capital = $data["Capital"];
        $this->Code2 = $data["Code2"];
        $this->Image1 = $data["Image1"];
        $this->Image2 = $data["Image2"];
        
        
    }
 
    function getCountry_Id() {
        return $this->Country_Id;
    }

    function getCode() {
        return $this->Code;
    }

    function getName() {
        return $this->Name;
    }

    function getContinent() {
        return $this->Continent;
    }

    function getRegion() {
        return $this->Region;
    }

    function getSurfaceArea() {
        return $this->SurfaceArea;
    }

    function getIndepYear() {
        return $this->IndepYear;
    }

    function getPopulation() {
        return $this->Population;
    }

    function getLifeExpectancy() {
        return $this->LifeExpectancy;
    }

    function getGNP() {
        return $this->GNP;
    }

    function getGNPOld() {
        return $this->GNPOld;
    }

    function getLocalName() {
        return $this->LocalName;
    }

    function getGovernementForm() {
        return $this->GovernementForm;
    }

    function getHeadOfState() {
        return $this->HeadOfState;
    }

    function getCapital() {
        return $this->Capital;
    }

    function getCode2() {
        return $this->Code2;
    }

    function getImage1() {
        return $this->Image1;
    }

    function getImage2() {
        return $this->Image2;
    }

    function setCountry_Id($Country_Id) {
        $this->Country_Id = $Country_Id;
        return $this;
    }

    function setCode($Code) {
        $this->Code = $Code;
        return $this;
    }

    function setName($Name) {
        $this->Name = $Name;
        return $this;
    }

    function setContinent($Continent) {
        $this->Continent = $Continent;
        return $this;
    }

    function setRegion($Region) {
        $this->Region = $Region;
        return $this;
    }

    function setSurfaceArea($SurfaceArea) {
        $this->SurfaceArea = $SurfaceArea;
        return $this;
    }

    function setIndepYear($IndepYear) {
        $this->IndepYear = $IndepYear;
        return $this;
    }

    function setPopulation($Population) {
        $this->Population = $Population;
        return $this;
    }

    function setLifeExpectancy($LifeExpectancy) {
        $this->LifeExpectancy = $LifeExpectancy;
        return $this;
    }

    function setGNP($GNP) {
        $this->GNP = $GNP;
        return $this;
    }

    function setGNPOld($GNPOld) {
        $this->GNPOld = $GNPOld;
        return $this;
    }

    function setLocalName($LocalName) {
        $this->LocalName = $LocalName;
        return $this;
    }

    function setGovernementForm($GovernementForm) {
        $this->GovernementForm = $GovernementForm;
        return $this;
    }

    function setHeadOfState($HeadOfState) {
        $this->HeadOfState = $HeadOfState;
        return $this;
    }

    function setCapital($Capital) {
        $this->Capital = $Capital;
        return $this;
    }

    function setCode2($Code2) {
        $this->Code2 = $Code2;
        return $this;
    }

    function setImage1($Image1) {
        $this->Image1 = $Image1;
        return $this;
    }

    function setImage2($Image2) {
        $this->Image2 = $Image2;
        return $this;
    }
}
    
    

