<?php

require_once '../src/model/Model.php';
require_once '../src/model/City.php';
require_once '../src/model/DAO.php';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class DAOCity extends DAO {
    
        public function find($id): City {

        $sql = "SELECT * FROM city WHERE City_Id = :id";
        $prepareStatement = $this->cnx->prepare($sql);
        $prepareStatement->bindValue("id", $id);
        $prepareStatement->execute();
        $city = $prepareStatement->fetchObject("City");
        return $city;
    }

    public function save($country) : void
    {
        $SQL = "INSERT INTO city (Name, CountryCode, District, Population) VALUES (:Name, :CountryCode, :District, :Population)";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("City_Id", $city->getCity_Id());
        $preparedStatement->bindValue("Name", $city->getName());
        $preparedStatement->bindValue("CountryCode", $city->getCountryCode());
        $preparedStatement->bindValue("District", $city->getDistrict());
        $preparedStatement->bindValue("Population", $city->getPopulation());
        $preparedStatement->execute();
    }
    /**
     * Met a jour un pays a partir de son id
     * @param Country $country
     * @return Void
     */
    public function update($country): void
    {
        $SQL = "UPDATE city SET City_Id = :City_Id, Name = :Name, CountryCode = :CountryCode, District = :District, Population = :Population WHERE City_Id = :City_Id";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("City_Id", $city->getCity_Id());
        $preparedStatement->bindValue("Name", $city->getName());
        $preparedStatement->bindValue("CountryCode", $city->getCountryCode());
        $preparedStatement->bindValue("District", $city->getDistrict());
        $preparedStatement->bindValue("Population", $city->getPopulation());
        $preparedStatement->execute();
    }

    public function remove($id): void {
        $SQL = "DELETE FROM city WHERE city_id= :id";

        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("id", $id);
        $preparedStatement->execute();
    }
    
    /**
     * 
     * @return array
     */
    public function findAll(): array {

        $sql = "SELECT * FROM city";
        $prepareStatement = $this->cnx->prepare($sql);
        $prepareStatement->execute();
        $cities = $prepareStatement->fetchAll(PDO::FETCH_ASSOC);
        return $cities;
    }
    
    /**
     * 
     * @return int
     */
    public function count(): int {

        $sql = "SELECT COUNT(City_Id) FROM city";
        $prepareStatement = $this->cnx->prepare($sql);
        $prepareStatement->execute();
        $count = $prepareStatement->fetchColumn();
        return $count;
    }
    
    public function FindCitiesByCountry($country) {
        
        $sql = "SELECT * FROM city WHERE CountryCode = (SELECT Code FROM country WHERE Name = :country)";
        $prepareStatement = $this->cnx->prepare($sql);
        $prepareStatement->bindValue("country", $country);
        $prepareStatement->execute();
        $city = $prepareStatement->fetchAll();
        return $city;
    }
    
    public function FindCitiesByCity($cities) {
        
        $sql = "SELECT * FROM cities WHERE Name = :cities";
        $prepareStatement = $this->cnx->prepare($sql);
        $prepareStatement->bindValue("cities", $cities);
        $prepareStatement->execute();
        $city = $prepareStatement->fetchAll();
        return $city;
    }
        
}