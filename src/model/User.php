<?php
require_once 'Model.php';

/**
 * Description of User
 *
 * @author student
 */
class User extends Model{
    protected $id;
    protected $login;
    protected $password;
    protected $nom;
    protected $idRole;
    
    public function __construct($id, $login, $password, $nom, $idRole) {
        $this->id = $id;
        $this->login = $login;
        $this->password = $password;
        $this->nom = $nom;
        $this->idRole = $idRole;
    }

    function getId() {
        return $this->id;
    }

    function getLogin() {
        return $this->login;
    }

    function getPassword() {
        return $this->password;
    }

    function getNom() {
        return $this->nom;
    }

    function getIdRole() {
        return $this->idRole;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setLogin($login) {
        $this->login = $login;
        return $this;
    }

    function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    function setNom($nom) {
        $this->nom = $nom;
        return $this;
    }

    function setIdRole($idRole) {
        $this->idRole = $idRole;
        return $this;
    } 
}
