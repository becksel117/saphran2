<?php

require_once 'Model.php';
require_once 'City.php';

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


abstract class DAO {
    
    /** @var $cnx PDO */
    protected $cnx; 
    
    public function __construct($cnx) {
        $this->cnx = $cnx;
        
    }
    // Recupere un objet (une ville) par son id et le renvoie 
    public function find($id) {
        
    }
    
    //Sauvegarde une entité
    public function save($entity) : void {
        
    }
    
    public function update($entity) :void {
        
    }
    
    public function remove($entity) : void {
        
    }
    
    public function findAll() : Array {
        
    }
    //renvoie le nombre d'éléments dans la base de données
    public function count() : int {
        
    }  
    
}