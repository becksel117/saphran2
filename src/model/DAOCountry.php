<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once '../src/model/Country.php';
require_once '../src/model/Model.php';
require_once '../src/model/DAO.php';

class DAOCountry extends DAO {

    public function __construct(PDO $cnx) {
        parent::__construct($cnx);
    }
    
    /**
     * 
     * @param type $id
     * @return \Country
     */
    public function find($id): Country {

        $sql = "SELECT * FROM country WHERE Name = :id";
        $prepareStatement = $this->cnx->prepare($sql);
        $prepareStatement->bindValue("id", $id);
        $prepareStatement->execute();
        $country = $prepareStatement->fetchObject("Country");
        return $country;
    }

    public function save($country) : void
    {
        $SQL = "INSERT INTO country (Code, Name, Continent, Region, SurfaceArea, IndepYear, Population, LifeExpectancy, GNP, GNPOld, LocalName, GovernmentForm, HeadOfState, Capital, Code2, Image1, Image2) VALUES (:Code, :Name, :Continent, :Region, :SurfaceArea, :IndepYear, :Population, :LifeExpectancy, :GNP, :GNPOld, :LocalName, :GovernmentForm, :HeadOfState, :Capital, :Code2, :Image1, :Image2)";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("Code", $country->getCode());
        $preparedStatement->bindValue("Name", $country->getName());
        $preparedStatement->bindValue("Continent", $country->getContinent());
        $preparedStatement->bindValue("Region", $country->getRegion());
        $preparedStatement->bindValue("SurfaceArea", $country->getSurfaceArea());
        $preparedStatement->bindValue("IndepYear", $country->getInDepYear());
        $preparedStatement->bindValue("Population", $country->getPopulation());
        $preparedStatement->bindValue("LifeExpectancy", $country->getLifeExpectancy());
        $preparedStatement->bindValue("GNP", $country->getGNP());
        $preparedStatement->bindValue("GNPOld", $country->getGNPOld());
        $preparedStatement->bindValue("LocalName", $country->getLocalName());
        $preparedStatement->bindValue("GovernmentForm", $country->getGovernmentForm());
        $preparedStatement->bindValue("HeadOfState", $country->getHeadOfState());
        $preparedStatement->bindValue("Capital", $country->getCapital());
        $preparedStatement->bindValue("Code2", $country->getCode2());
        $preparedStatement->bindValue("Image1", $country->getImage1());
        $preparedStatement->bindValue("Image2", $country->getImage2());
        $preparedStatement->execute();
    }
    /**
     * Met a jour un pays a partir de son id
     * @param Country $country
     * @return Void
     */
    public function update($country): void
    {
        $SQL = "UPDATE country SET Country_Id = :Country_Id, Code = :Code, Name = :Name, Continent = :Continent, Region = :Region, SurfaceArea = :SurfaceArea, IndepYear = :IndepYear, Population = :Population, LifeExpectancy = :LifeExpectancy, GNP = :GNP, GNPOld = :GNPOld, LocalName = :LocalName, GovernmentForm = :GovernmentForm, HeadOfState = :HeadOfState, Capital = :Capital, Code2 = :Code2, Image1 = :Image1, Image2 = :Image2 WHERE Country_Id = :Country_Id";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("Country_Id", $country->getCountry_Id());
        $preparedStatement->bindValue("Code", $country->getCode());
        $preparedStatement->bindValue("Name", $country->getName());
        $preparedStatement->bindValue("Continent", $country->getContinent());
        $preparedStatement->bindValue("Region", $country->getRegion());
        $preparedStatement->bindValue("SurfaceArea", $country->getSurfaceArea());
        $preparedStatement->bindValue("IndepYear", $country->getInDepYear());
        $preparedStatement->bindValue("Population", $country->getPopulation());
        $preparedStatement->bindValue("LifeExpectancy", $country->getLifeExpectancy());
        $preparedStatement->bindValue("GNP", $country->getGNP());
        $preparedStatement->bindValue("GNPOld", $country->getGNPOld());
        $preparedStatement->bindValue("LocalName", $country->getLocalName());
        $preparedStatement->bindValue("GovernmentForm", $country->getGovernmentForm());
        $preparedStatement->bindValue("HeadOfState", $country->getHeadOfState());
        $preparedStatement->bindValue("Capital", $country->getCapital());
        $preparedStatement->bindValue("Code2", $country->getCode2());
        $preparedStatement->bindValue("Image1", $country->getImage1());
        $preparedStatement->bindValue("Image2", $country->getImage2());
        $preparedStatement->execute();
    }

    public function remove($id): void {

        $sqlCity = "DELETE FROM city WHERE CountryCode = (SELECT Code FROM country WHERE Country_Id = :Country_Id)";
        $prepareStatementCity = $this->cnx->prepare($sqlCity);
        $prepareStatementCity->bindValue("Country_Id", $id);
        $prepareStatementCity->execute();

        $sqlLanguage = "DELETE FROM countrylanguage WHERE CountryCode = (SELECT Code FROM country WHERE Country_Id = :Country_Id)";
        $prepareStatementLanguage = $this->cnx->prepare($sqlLanguage);
        $prepareStatementLanguage->bindValue("Country_Id", $id);
        $prepareStatementLanguage->execute();

        $sqlCountry = "DELETE FROM country WHERE Country_Id = :Country_Id";
        $prepareStatementCountry = $this->cnx->prepare($sqlCountry);
        $prepareStatementCountry->bindValue("Country_Id", $id);
        $prepareStatementCountry->execute();
    }
    
    /**
     * 
     * @return array
     */
    public function findAll(): array {

        $sql = "SELECT * FROM country";
        $prepareStatement = $this->cnx->prepare($sql);
        $prepareStatement->execute();
        $countries = $prepareStatement->fetchAll(PDO::FETCH_ASSOC);
        return $countries;
    }
    
    /**
     * 
     * @return int
     */
    public function count(): int {

        $sql = "SELECT COUNT(Country_Id) FROM country";
        $prepareStatement = $this->cnx->prepare($sql);
        $prepareStatement->execute();
        $count = $prepareStatement->fetchColumn();
        return $count;
    }
    
    public function findContinent() {
        
        $SQL = "SELECT DISTINCT continent FROM country";
        $preparedStatement = $this->cnx->query($SQL);
        $preparedStatement->execute();
        $continent = $preparedStatement->fetchAll(PDO::FETCH_ASSOC);
        return $continent;
    
    }
    
    public function FindCountriesByContinent($Continent) {
        
        $sql = "SELECT * FROM country WHERE Continent= :Continent";
        $prepareStatement = $this->cnx->prepare($sql);
        $prepareStatement->bindValue("Continent", $Continent);
        $prepareStatement->execute();
        $country = $prepareStatement->fetchAll();
        return $country;
    }
    
    public function FindCountryByCountry($country) {
        
        $sql = "SELECT * FROM country WHERE Name = :country";
        $prepareStatement = $this->cnx->prepare($sql);
        $prepareStatement->bindValue("country", $country);
        $prepareStatement->execute();
        $countries = $prepareStatement->fetchAll();
        return $countries;
    }

}




















