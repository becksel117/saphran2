<?php

require_once 'DAO.php';
require_once 'User.php';

/**
 * Description of DAOUser
 *
 * @author student
 */
class DAOUser extends DAO {
    public function __construct(PDO $cnx) {
        parent::__construct($cnx);
    }
    
    public function findUserByName($nom) {
        $sql = "SELECT * FROM USERS WHERE nom= :nom";
        $prepareStatement = $this->cnx->prepare($sql);
        $prepareStatement->bindValue("nom", $nom);
        $prepareStatement->execute();
        $user = $prepareStatement->fetchAll();
        return $user;
    }
    
    public function count(): int {

        $sql = "SELECT COUNT(idUser) FROM country";
        $prepareStatement = $this->cnx->prepare($sql);
        $prepareStatement->execute();
        $count = $prepareStatement->fetchColumn();
        return $count;
    }
    
     public function save($user) : void
    {
        $SQL = "INSERT INTO USERS (idUser, login, passwd, nom, idRole) VALUES (:idUser, :login, :passwd, :nom, :idRole)";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("idUser", $user->getId());
        $preparedStatement->bindValue("login", $user->getId());
        $preparedStatement->bindValue("passwd", $user->getId());
        $preparedStatement->bindValue("nom", $user->getId());
        $preparedStatement->bindValue("idRole", $user->getId());
        $preparedStatement->execute();
    }
}
