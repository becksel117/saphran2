<?php
require_once '../src/model/Model.php';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class City extends Model {
    protected $City_Id;
    protected $Name;
    protected $CountryCode;
    protected $District;
    protected $Population;
    
    /**
     * 
     * @param array $data
     */
    public function __construct(array $data = null) {
        parent::__construct();
        $this->City_Id = $data["City_Id"];
        $this->Name = $data["Name"];
        $this->CountryCode = $data["Country_Code"];
        $this->District = $data["District"];
        $this->Population = $data["Population"];
    }
    
    function getCity_Id() {
        return $this->City_Id;
    }

    function getName() {
        return $this->Name;
    }

    function getCountryCode() {
        return $this->CountryCode;
    }

    function getDistrict() {
        return $this->district;
    }

    function getPopulation() {
        return $this->population;
    }

    function setCity_Id($City_Id) {
        $this->City_Id = $City_Id;
        return $this;
    }

    function setName($Name) {
        $this->Name = $Name;
        return $this;
    }

    function setCountryCode($CountryCode) {
        $this->CountryCode = $CountryCode;
        return $this;
    }

    function setDistrict($district) {
        $this->district = $district;
        return $this;
    }

    function setPopulation($population) {
        $this->population = $population;
        return $this;
    }

    
}