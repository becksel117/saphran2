<?php

require_once '../src/controllers/CityController.php';
require_once '../src/model/City.php';
require_once '../src/model/Country.php';
require_once '../src/model/DAOCountry.php';
require_once '../src/utils/SingletonDatabase.php';
require_once '../src/utils/SingletonConfigReader.php';
require_once '../src/controllers/CountryController.php';
require_once '../src/controllers/DefaultController.php';

//echo "<pre>" . print_r($_SERVER, true) . "<pre>";

if (isset($_SERVER["PATH_INFO"])) {
    $path = trim($_SERVER["PATH_INFO"], "/");
} else {
    $path = "";
}

$fragments = explode("/", $path);

//var_dump($fragment);

$control = array_shift($fragments);

switch ($control) {
   
    case "city" : {
            
            //calling function to prevend all hard code here
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                cityRoutes_get($fragments);
            }
            break;
        }
    case "country" : {
                
           if ($_SERVER["REQUEST_METHOD"] == "GET") {
               countryRoutes_get($fragments);
           }
           if ($_SERVER["REQUEST_METHOD"] == "POST") {
               countryRoutes_post($fragments);
           }
            break;
        
        }
    default : {
            $DefaultController = new DefaultController();
            $DefaultController->show();
            break;
        }
}

function cityRoutes_get($fragments) {

    //var_dump($fragment);

    $action = array_shift($fragments);
    
    //var_dump($action);

    switch ($action) {
        
        case "show" : {
                
                
                call_user_func_array([new CityController(), "display"], $fragments);
                break;
            }
        case "demo" : {
                
                
                
                call_user_func_array(["CityController", "demo_test"], $fragments);
                break;
            }
        case "update" : {
                call_user_func_array([new CityController(), "update"], $fragments);
                break;
            }
        case "delete" : {
                call_user_func_array([new CityController(), "delete"], $fragments);
                break;
            }
        case "add" : {
                call_user_func_array([new CityController(), "add"], $fragments);
                break;
            }
        default : {
                echo 'DEFAULT CITY';
                //Gestion du probleme
            }
    }
}

function countryRoutes_get($fragments) {

    //var_dump($fragment);

    $action = array_shift($fragments);
    
    //var_dump($action);

    switch ($action) {
        case "show" : {
                call_user_func_array([new CountryController(), "display"], $fragments);
                break;
            }
        case "demo" : {
                
                
                //var_dump($fragments);
                call_user_func_array(["CountryController", "demo_test"], $fragments);
                break;
            }
        case "update" : {
                call_user_func_array([new CountryController(), "update"], $fragments);
                break;
            }
        case "delete" : {
                call_user_func_array([new CountryController(), "delete"], $fragments);
                break;
            }
        case "doDelete" : {
                call_user_func_array([new CountryController(), "doDelete"], $fragments);
                break;
            }
        case "add" : {
                call_user_func_array([new CountryController(), "add"], $fragments);
                break;
            }
        default : {
               echo 'DEFAULT COUNTRY';
                //Gestion du probleme
            }
       
    }
}
    function countryRoutes_post($fragments) {

    //var_dump($fragment);

    $action = array_shift($fragments);
    
    //var_dump($action);

    switch ($action) {
        
        case "doDelete" : {
                call_user_func_array([new CountryController(), "doDelete"], $fragments);
                break;
            }
        
    }
    
}